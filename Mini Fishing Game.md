Game idea: basic fishing micro game

To do:

- [x] [Fishing game Ref board](obsidian://open?vault=GameDev&file=Excalidraw%2FFishing%20game%20Ref%20boar%202023-11-05%2018.24.43.excalidraw)
- [x] [state machine for fishing logic](obsidian://open?vault=GameDev&file=Excalidraw%2FDrawing%202023-11-06%2019.37.21.excalidraw)
- [ ] Fishing assets
- [ ] UI
- [ ] Main Menu
- [ ] Player stats/Leader board


Follow up ideas:
- Minecraft fishing clone (Just to learn about it's fishing mechanics)

bait?:
- Worms
- cheese
- pineapple